local VelocityGenerator = require 'game.components.velocity'
local PlayerInputGenerator = require 'game.components.player_input'

return function(layer, index)
  local player_data = layer.objects[index]
  local image = love.graphics.newImage('game/assets/images/player.png')
  local imgH = image:getHeight()
  local imgW = image:getWidth()

  return {
		x = player_data.x,
		y = player_data.y,
		w = player_data.width,
		h = player_data.height,
    sprite = {
      image = image,
      quads = {
        down = love.graphics.newQuad(0, 0, 16, 16, imgW, imgH),
        left = love.graphics.newQuad(16, 0, 16, 16, imgW, imgH),
        up = love.graphics.newQuad(32, 0, 16, 16, imgW, imgH),
        right = love.graphics.newQuad(48, 0, 16, 16, imgW, imgH),
      }
    },
    facing = 'down',
    velocity = VelocityGenerator(layer.properties.speed),
    input = PlayerInputGenerator({
      up = 'up',
      down = 'down',
      left = 'left',
      right = 'right',
      pause = 'p',
      action = 'space',
    }),
  }
end
