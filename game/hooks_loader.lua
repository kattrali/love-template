--[[
Load action hooks for an object in a map
]]
return function(level_index, map)
  local level_name = string.format("level%02d", level_index)
  local function add_helpers(name, hooks)
    function hooks:property(prop_name)
      local layer = map.layers['sprites']
      return layer.triggers[name][prop_name]
    end

    function hooks:set_property(prop_name, value)
      local layer = map.layers['sprites']
      layer.triggers[name][prop_name] = value
    end

    function hooks:lookup_targets()
      local layer = map.layers['sprites']
      local data = layer.triggers[name].targets
      if data then
        local targets = {}
        for target in string.gmatch(data, "[^%W]+") do
          table.insert(targets, self:lookup_object(target))
        end
        return targets
      end
    end

    function hooks:lookup_object(id)
      return map.layers['sprites'].triggers[id]
    end
  end

  local loader = {}
  function loader:load_hooks(name)
    local path = 'game.assets.maps.' .. level_name .. '.' .. name
    local status, hooks = pcall(require, path)
    if status then
      add_helpers(name, hooks)
      return hooks
    end

    return nil
  end

  return loader
end
