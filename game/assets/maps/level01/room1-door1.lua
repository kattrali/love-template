local hooks = {}

function hooks:update()
  if self:property('state') == 'remove' then
    return
  end

  local switch1 = self:lookup_object('room1-switch1')
  if switch1.state == 'on' then
    self:set_property('state', 'remove')
  end
end

return hooks
