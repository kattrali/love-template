local hooks = {}

function hooks:action()
  if self:property('state') == 'off' then
    self:set_property('state', 'on')
  else
    self:set_property('state', 'off')
  end
end

return hooks
