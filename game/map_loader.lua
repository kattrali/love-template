local sti = require 'lib.sti'
local Player = require 'game.objects.player'


local function load_map_objects(map, player)
  map:addCustomLayer("sprites", 3)
  local sprite_layer = map.layers["sprites"]
  sprite_layer.player = player
  sprite_layer.triggers = {}
  for _, trigger in ipairs(map.layers['triggers'].objects) do
    sprite_layer.triggers[trigger.name] = {
      type = trigger.type,
      name = trigger.name,
      x = trigger.x,
      y = trigger.y,
      w = trigger.width,
      h = trigger.height,
    }
    for k,v in pairs(trigger.properties) do
      sprite_layer.triggers[trigger.name][k] = v
    end
  end

  function sprite_layer:draw()
    for _, trigger in pairs(self.triggers) do
      if trigger.type == 'door' then
        love.graphics.setColor(80, 0, 5)
        love.graphics.rectangle('fill', trigger.x, trigger.y, trigger.w, trigger.h)
      elseif trigger.type == 'switch' then
        if trigger.state == 'off' then
          love.graphics.setColor(0, 0, 0)
        else
          love.graphics.setColor(255, 255, 255)
        end
        love.graphics.rectangle('fill', trigger.x, trigger.y, trigger.w, trigger.h)
      end
    end
    love.graphics.setColor(255, 255, 255)
    local obj = self.player
    local x = math.floor(obj.x)
    local y = math.floor(obj.y)
    love.graphics.draw(obj.sprite.image, obj.sprite.quads[obj.facing], x, y)
  end
end

return function(map_path)
  local map = sti(map_path)
  local player = Player(map.layers["player"], 1)
  load_map_objects(map, player)

  return { map = map, player = player }
end
