local systems = {}

-- Loop over every system, invoking a function by name if present
local function system_func(name, ...)
  for i, system in ipairs(systems) do
    if type(system[name]) == 'function' then
      system[name](...)
    end
  end
end

-- Remove a system from being run
function systems.remove(systemToPop)
  for i, system in ipairs(systems) do
    if system == systemToPop then
      systems[i + 1] = nil
      break
    end
  end
end

-- Add a system to run
function systems.add(systemToAdd)
  for i, system in ipairs(systems) do
    if system == nil then
      systems[i] = systemToAdd
      return
    end
  end
  systems[#systems + 1] = systemToAdd
end

-- Run update loops on each system
function systems.update(levelstate, dt)
  system_func('preUpdate', levelstate, dt)
  system_func('update', levelstate, dt)
  system_func('postUpdate', levelstate, dt)
end

-- Run keyreleased loop on each system
function systems.keyreleased(levelstate, key, code)
  system_func('keyreleased', levelstate, key, code)
end

-- Run draw loops on each system
function systems.draw(levelstate, dt)
  system_func('draw', levelstate, dt)
end


return systems
