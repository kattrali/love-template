return function()
  local system = {}

  function system.keyreleased(levelstate, key, code)
    local entity = levelstate.player
    if key == entity.input.keymap.pause then
      levelstate.paused = not levelstate.paused
      return
    end
  end

  function system.update(levelstate, dt)
    local entity = levelstate.player
    if levelstate.paused then
      return
    end

    if entity.input.moveUp() then
      entity.facing = "up"
      entity.velocity.dy = -entity.velocity.speed * dt
    elseif entity.input.moveDown() then
      entity.facing = "down"
      entity.velocity.dy = entity.velocity.speed * dt
    end

    if entity.input.moveLeft() then
      entity.facing = "left"
      entity.velocity.dx = -entity.velocity.speed * dt
    elseif entity.input.moveRight() then
      entity.facing = "right"
      entity.velocity.dx = entity.velocity.speed * dt
    end

    if entity.input.doAction() then
      entity.action = true
    end
  end

  function system.postUpdate(levelstate, dt)
    levelstate.player.velocity.dx = 0
    levelstate.player.velocity.dy = 0
    levelstate.player.action = false
  end

  return system
end
