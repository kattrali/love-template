local bump = require 'lib.bump'

return function (player, map)
  local system = {}
  local world = bump.newWorld(map.tilewidth)

  -- Add player to physics world
  world:add(player, player.x, player.y, player.w, player.h)

  -- Add all rectangles on the "collideables" layer of `map`
  for _, obj in ipairs(map.layers["collideables"].objects) do
    if obj.shape == "rectangle" then
      world:add(obj, obj.x, obj.y, obj.width, obj.height)
    end
  end
  for _, obj in pairs(map.layers["sprites"].triggers) do
    world:add(obj, obj.x, obj.y, obj.w, obj.h)
  end

  function system.update(levelstate, dt)
    for _, obj in pairs(levelstate.map.layers["sprites"].triggers) do
      if obj.type ~= nil and obj.state == 'remove' then
        world:remove(obj)
        obj.state = 'deleted'
        obj.type = nil
      end
    end

    if levelstate.player.velocity.dx ~= 0 or levelstate.player.velocity.dy ~= 0 then
      local entity = levelstate.player
      local goalX = entity.x + entity.velocity.dx
      local goalY = entity.y + entity.velocity.dy
      local actualX, actualY, cols, _ = world:move(entity, goalX, goalY)

      entity.x = actualX
      entity.y = actualY
      entity.collisions = cols
    end
  end

  return system
end
