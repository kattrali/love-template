return function (hooks_loader)
  local system = { trigger_cooldown = 0 }

  local function run_action_hook(target)
    local hooks = hooks_loader:load_hooks(target.name)
    if hooks ~= nil then
      hooks:action()
      if target.targets ~= nil then
        for t in string.gmatch(target.targets, "[^,]+") do
          local thooks = hooks_loader:load_hooks(t)
          thooks:update()
        end
      end
    end
  end

  function system.update(levelstate, dt)
    system.trigger_cooldown = system.trigger_cooldown - dt

    local p = levelstate.player
    if p.collisions ~= nil and #p.collisions > 0 and p.action then
      for _, col in ipairs(p.collisions) do
        if col.other.actionable and system.trigger_cooldown <= 0 then
          system.trigger_cooldown = 0.25
          run_action_hook(col.other)
        end
      end
    end
  end

  return system
end
