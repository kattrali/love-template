local Camera = require 'lib.camera'

return function ()
  local system = {}
  local camera = Camera()
  camera:setFollowStyle('TOPDOWN')

  function system.update(levelstate, dt)
    levelstate.map:update(dt)
    camera:update(dt)
    camera:follow(levelstate.player.x, levelstate.player.y)
  end

  -- Renders map centering on player. Avoids going over the map edges.
  function system.draw(levelstate, dt)
    local windowWidth  = love.graphics.getWidth()
    local windowHeight = love.graphics.getHeight()
    local mapMaxWidth = levelstate.map.width * levelstate.map.tilewidth
    local mapMaxHeight = levelstate.map.height * levelstate.map.tileheight
    local x = math.min(math.max(0, camera.x - windowWidth/2), mapMaxWidth - windowWidth)
    local y = math.min(math.max(0, camera.y - windowHeight/2), mapMaxHeight - windowHeight)

    levelstate.map:draw(-x, -y, 1, 1)
  end

  return system
end
