local Gamestate = require 'lib.gamestate'
local RenderMapSystem = require 'game.systems.render_map'
local PlayerInputSystem = require 'game.systems.player_input'
local MovementSystem = require 'game.systems.movement'
local TriggerSystem = require 'game.systems.trigger'
local MapLoader = require 'game.map_loader'
local HooksLoader = require 'game.hooks_loader'
local SceneState = require 'game.states.scene'


local state = {}

function state:enter(_, level_index)
  self.level_index = level_index
  Gamestate.push(SceneState, "game/assets/scenes/intro.scene")
end

function state:resume(prev)
  if prev.__animation == true then
    local map_path = string.format("game/assets/maps/level%02d/init.lua", self.level_index)
    self.levelstate = MapLoader(map_path)
    self.hooks_loader = HooksLoader(self.level_index, self.levelstate.map)

    self.systems = require 'game.systems'
    self.systems.add(PlayerInputSystem())
    self.systems.add(RenderMapSystem())
    self.systems.add(MovementSystem(self.levelstate.player, self.levelstate.map))
    self.systems.add(TriggerSystem(self.hooks_loader))
  end
end

function state:draw()
  local dt = love.timer.getDelta()
  self.systems.draw(self.levelstate, dt)
end

function state:update(dt)
  self.systems.update(self.levelstate, dt)
end

function state:keydown(key, code)
end

function state:keyreleased(key, code)
end

return state
