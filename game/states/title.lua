local Gamestate = require 'lib.gamestate'
local LevelState = require 'game.states.level'

local state = {}
local font = love.graphics.newFont('game/assets/fonts/shattered-v1.ttf', 24)
local elapsed = 0
local BLINK_INTERVAL = 25

function state:enter()
  love.graphics.setFont(font)
end

function state:draw()
  if elapsed % 100 > BLINK_INTERVAL then
    love.graphics.print("> Press SPACE to continue <", 168, 360)
  end
end

function state:update(dt)
  elapsed = elapsed + math.floor(dt * 100)
end

function state:keyreleased(key, code)
  if key == 'space' then
    Gamestate.push(LevelState, 1)
  end
end

return state
