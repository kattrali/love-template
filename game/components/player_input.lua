return function (keymap)
  local component = {}

  component.moveUp = function()
    return love.keyboard.isDown(keymap.up) and not love.keyboard.isDown(keymap.down)
  end

  component.moveDown = function()
    return love.keyboard.isDown(keymap.down) and not love.keyboard.isDown(keymap.up)
  end

  component.moveLeft = function()
    return love.keyboard.isDown(keymap.left) and not love.keyboard.isDown(keymap.right)
  end

  component.moveRight = function()
    return love.keyboard.isDown(keymap.right) and not love.keyboard.isDown(keymap.left)
  end

  component.doAction = function()
    return love.keyboard.isDown(keymap.action)
  end

  return component
end
