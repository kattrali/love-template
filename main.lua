local Gamestate = require 'lib.gamestate'
local title = require 'game.states.title'

function love.load()
  love.graphics.setDefaultFilter("nearest", "nearest")
  Gamestate.registerEvents()
  Gamestate.switch(title)
end
