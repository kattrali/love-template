-- Map object hooks
--
-- Helpers:
--   self:property(name) - get sprite property
--   self:set_property(name, value) - set sprite property
--   self:lookup_object(name) - get sprite by name
--   self:lookup_targets() - get sprites from `targets` map property
local hooks = {}

-- Respond to a target invocation
--function hooks:update()
--end

-- If map property actionable == true, respond to collision + player action
--function hooks:action()
--end

return hooks
