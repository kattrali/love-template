local Gamestate = require 'lib.gamestate'

local state = {}

function state:enter()
end

function state:draw()
end

function state:update(dt)
end

function state:keydown(key, code)
end

function state:keyreleased(key, code)
end

return state
