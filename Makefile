all: build

# Won't work if bash is old af:
SHELL=/usr/bin/env bash -O globstar
SRCS=LICENSE main.lua conf.lua $(shell ls {game,lib}/{**/,}*.*)
TILED:=~/bin/apps/Tiled.app/Contents/MacOS/Tiled
LOVE:=/usr/local/bin/love

# ~ Packaging:
TARGET=target
WINLOVEDIR:=../love-win32
WINLOVE=$(WINLOVEDIR)/love.exe
WINLIBS=$(patsubst $(WINLOVEDIR)/%,$(TARGET)/%,$(wildcard $(WINLOVEDIR)/*.dll))
MACLOVE:=~/bin/apps/love.app
GAME=$(shell perl -lne 'print $$1 if /title = "(.*)"/' conf.lua)
space :=
space +=
SAFEGAME=$(subst $(space),-,$(GAME))
BUNDLEID=me.delisa.$(SAFEGAME)

ZIPFILE=$(TARGET)/$(SAFEGAME).love
WINZIPFILE=$(TARGET)/$(SAFEGAME)_win.zip
MACZIPFILE=$(TARGET)/$(SAFEGAME)_mac.zip

$(ZIPFILE): $(SRCS)
	@mkdir -p $(TARGET)
	@zip -9rq "$@" $^

$(TARGET)/%.dll: $(WINLOVEDIR)/%.dll
	@install $< $@

$(TARGET)/$(SAFEGAME).exe: $(ZIPFILE)
	@cat $(WINLOVE) "$(ZIPFILE)" > $@

$(TARGET)/$(SAFEGAME).app: $(ZIPFILE)
	@cp -R $(MACLOVE) $@
	@cp $(ZIPFILE) $@/Contents/Resources
	@/usr/libexec/PlistBuddy \
		-c "Delete UTExportedTypeDeclarations" \
		-c "Set CFBundleName '$(GAME)'" \
		-c "Set CFBundleIdentifier $(BUNDLEID)" $@/Contents/Info.plist

# See the guide for more info, etc:
# https://love2d.org/wiki/Game_Distribution#Linux_.2F_OS_X
$(WINZIPFILE): $(TARGET)/$(SAFEGAME).exe $(WINLIBS)
	@cd $(TARGET) && zip -9rq "$(shell basename $(WINZIPFILE))" *.dll $(SAFEGAME).exe

$(MACZIPFILE): $(TARGET)/$(SAFEGAME).app
	@cd $(TARGET) && zip -9rqy "$(shell basename $(MACZIPFILE))" "$(SAFEGAME).app"

# ~ end Packaging

.PHONY: run build add-map add-state add-system help test tiled-run \
	tiled-edit-hooks package package-win package-mac package-zip

run: build

build: ## Build and run the project
	@$(LOVE) .

clean: ## Remove build artifects
	@rm -rf $(TARGET)

help: ## Show help text
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

test: ## Lint and run tests
	@luacheck game/

package-zip: $(ZIPFILE) ## Generate .love game file

package-win: $(WINZIPFILE) ## Generate windows executable

package-mac: $(MACZIPFILE) ## Generate mac app

package: package-zip package-win package-mac ## Generate distribution binaries

add-map: ## Create a new map asset
ifeq ($(name),)
	@$(error name is not defined. Run with `make add-map name=[value]`)
endif
	@install .templates/map.tmx game/assets/maps/$(name).tmx

add-state: ## Create a new game state
ifeq ($(name),)
	@$(error name is not defined. Run with `make add-state name=[value]`)
endif
	@install .templates/state.lua game/states/$(name).lua

add-system: ## Create a new system
ifeq ($(name),)
	@$(error name is not defined. Run with `make add-system name=[value]`)
endif
	@install .templates/system.lua game/systems/$(name).lua

add-hooks: ## Create new map object hooks
ifeq ($(name),)
	@$(error name is not defined. Run with `make add-hooks name=[value] level=[value]`)
endif
ifeq ($(level),)
	@$(error level is not defined. Run with `make add-hooks name=[value] level=[value]`)
endif
	@mkdir -p game/assets/maps/$(level)
	@install .templates/hooks.lua game/assets/maps/$(level)/$(name).lua

# Companion to a Tiled command which, when an object is selected, opens (and
# creates if needed) a hooks script for interacting with map object properties.
# Uses .templates/hooks.lua to create a new file if it does not exist already.
#
# Tiled command:
# * Executable: `make tiled-edit-hooks mapfile=%mapfile layer=%layername id=%objectid`
# * Working directory: %mappath/../../..
#
# Dependencies:
# * yq (http://yq.readthedocs.io) - `brew install python-yq`
# * MacVim (http://macvim-dev.github.io) - `brew cask install macvim`
tiled-edit-hooks: ## Add and edit hoox for a map object from Tiled command
ifeq ($(mapfile),) # %mapfile
	@$(error mapfile is not defined. Run with `make add-hooks mapfile=[value] layer=[value] id=[value]`)
endif
ifeq ($(layer),) # %layername
	@$(error layer is not defined. Run with `make add-hooks mapfile=[value] layer=[value] id=[value]`)
endif
ifeq ($(id),) # %objectid
	@$(error id is not defined. Run with `make add-hooks mapfile=[value] layer=[value] id=[value]`)
endif
	@mapname=$(shell basename $(mapfile)); \
	level=$${mapname%.*}; \
	objname=$(shell xq '.map.objectgroup[] | select(.["@name"] == "$(layer)") | .object[] | select(.["@id"] == "$(id)") | .["@name"]' game/assets/maps/level01.tmx | tr -d '"'); \
	filepath=game/assets/maps/$${level}/$${objname}.lua; \
	test -f $${filepath} || $(MAKE) add-hooks name=$${objname} level=$${level}; \
	mvim game/assets/maps/$${level}/$${objname}.lua;

# Companion to a Tiled command which runs the game after exporting the map
# which is currently being edited.
#
# Tiled command:
# * Executable: `make tiled-run mapfile=%mapfile`
# * Working directory: %mappath/../../..
tiled-run: ## Run game after exporting a map
ifeq ($(mapfile),) # %mapfile
	@$(error mapfile is not defined. Run with `make tiled-run mapfile=[value]`)
endif
	@mapfilename=$(shell basename $(mapfile)); \
	dir=$${mapfilename%.*}; \
	exportdir=game/assets/maps/$${dir}; \
	exportfile=$${exportdir}/init.lua; \
	mkdir -p $${exportdir}; \
	$(TILED) --export-map lua "$(mapfile)" "$${exportfile}"; \
	$(MAKE) run
