top-down love template
======================

A 2D game template for love!

## Usage

1.  Copy this directory to your new game location

    ```bash
    cp -r love-template/ locklight-chronicles/
    ```

2.  Edit [conf.lua](conf.lua), adding your game name and tweaking
    [config options](https://love2d.org/wiki/Config_Files)
3.  Edit the map, level state, and systems to add game mechanics
4.  Polish with assets and juiciness
5.  Package it!
6.  [Shout about it!](https://github.com/pixelnest/presskit.html)

## States

Game behavior is divided by
[`hump.gamestate`](https://hump.readthedocs.io/en/latest/gamestate.html) tables.

On load, the [`title`](game/states/title.lua) state pushed onto the stack, which
in turn lists for the start input and pushes the
[`level`](game/states/level.lua) state. The core gameplay occurs in the level
state.

The [`scene`](game/states/scene.lua) state loads and plays cutscene files. There
is an example cutscene which plays before the first level in
[`intro.scene`](game/assets/scenes/intro.scene). Cutscene files use a DSL for
defining map, sprite, image, and audio interactions as groups of animations.

## Systems

Level logic is divided into systems which manipulate game data based on input
and time elapsed. Systems are controlled by a manager object which coordinates
callbacks.

```lua
local systems = require 'game.systems'
```

A system can implement and receive event callbacks for the following events:

* `update` - manage game state frame-to-frame (and optionally perform
  setup/teardown in `preUpdate` and `postUpdate`)
* `draw` - render content onscreen
* `keyreleased` - Handle a key release event

Every callback is optional.
Once implemented, it can be added to the manager:

```lua
local system = {
  update = function(state, dt)
    -- do stuff!
  end,

  draw = function(state, dt)
    -- draw stuff!
  end
}

systems.add(system)
```

The `state` argument is the data the system should manipulate.
System callbacks are run in the order they are added.
The following systems are included:

* [Render Map](game/systems/render_map.lua) - Renders a
  [Tiled](https://mapeditor.org) map onscreen, following the player with a
  camera
* [Movement](game/systems/movement.lua) - Manages collisions between objects and
  moves objects according to their velocity
* [Player Input](game/systems/player_input.lua) - Reads input devices updating
  the player velocity/direction and action button state
* [Trigger](game/systems/trigger.lua) - Responds to active action button state,
  using collision data to determine if an actionable object is in range,
  activating object callbacks if so.

## Libraries

The following libraries are included:

* [`anim8`](https://github.com/kikito/anim8) - frame-based animation
* [`bump`](https://github.com/kikito/bump.lua) - collision detection
* [`camera`/STALKER-X](https://github.com/SSYGEN/STALKER-X) - a camera module
  with lerp and lead and simple screen effects
* [`hump.gamestate`](https://hump.readthedocs.io/en/latest/gamestate.html) - game data and behavior encapsulation
* [`hump.signal`](https://hump.readthedocs.io/en/latest/signal.html) - Observers
* [`hump.timer`](https://hump.readthedocs.io/en/latest/timer.html) - function
  execution scheduling and tweening
* [`hump.vector`](https://hump.readthedocs.io/en/latest/vector.html) - 2D vector
  calculations
* [`sti`](https://github.com/karai17/Simple-Tiled-Implementation) - Tiled map
  loader and renderer

## Assets

In addition to the [fantastic default art](game/assets/images/player.png), this
template includes the
[Shattered](https://www.patreon.com/posts/shattered-sf-17775824) font by eeve
somepx.

## Tools

Run `make help` for a list of all (useful) commands and descriptions.

| Make target     | |
|-----------------|----------------------------|
|`build`/`run`    | Run the game               |
|`test`           | Lint and run tests, if any |

### Creating files

| Make target     | |
|-----------------|---------------------------------------------------------|
|`add-hooks`      | Creates a new trigger script from [a template](.templates/hooks.lua) |
|`add-map`        | Creates a new map from [a template](.templates/map.tmx) |
|`add-state`      | Creates a new gamestate from [a template](.templates/state.lua) |
|`add-system`     | Creates a new system from [a template](.templates/system.lua) |

### Tiled integration

The Makefile includes helpers for scripting and running the game from Tiled.
Additional documentation is available in the Makefile itself.

| Make target     | |
|-------------------|------------------------------------------------------|
|`tiled-run`        | Runs the game after exporting the map currently open |
|`tiled-edit-hooks` | Creates and/or opens the trigger script for the object selected in the map |

### Export/Packaging

| Make target     | |
|-------------------|------------------------------------------------------|
|`package`          | Build game distribution files for mac, windows, and linux |


## Resources

### Planning

* Pencil and cheap paper for planning, doodling, storyboarding
* [One game a month!](http://onegameamonth.com)
* [itch.io](https://itch.io) for [asset packs](https://itch.io/game-assets),
  [game jams](https://itch.io/jams), and [tools](https://itch.io/tools).

### Coding

* [love2d wiki](https://love2d.org/wiki/Main_Page)

### Assets

* [aseprite](http://aseprite.org) for pixel art editing
* [bfxr](http://bfxr.net) for sound effects
* [milkytracker](http://milkytracker.titandemo.org) for chiptunes
* [musescore](http://musescore.com) for sheet music editing
* [pixel art palettes and online editor](https://lospec.com/palette-list)
* [Adam Saltsman's public domain tilesets](https://adamatomic.itch.io)
* [Pedro Medeiros' pixel art tutorials](https://www.patreon.com/saint11), [Mort
  Mort's pixel art tutorials](https://www.youtube.com/user/atMNRArt)
* [Free sound effects](https://freesound.org) and
  [music](https://freesound.org/browse/tags/music/)

### Packaging

* [Love Game Distribution guide](https://love2d.org/wiki/Game_Distribution)
* [presskit.html](https://github.com/pixelnest/presskit.html)
